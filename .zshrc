#!/usr/bin/zsh

# some great resources:
# zsh docs (multiple chapters): https://zsh.sourceforge.io/Doc/Release/Prompt-Expansion.html
# epic blog post: https://scriptingosx.com/2019/07/moving-to-zsh-06-customizing-the-zsh-prompt/
# color cheat sheet: https://jonasjacek.github.io/colors/
# git stuff: https://arjanvandergaag.nl/blog/customize-zsh-prompt-with-vcs-info.html

# setup git information
autoload -Uz vcs_info
precmd_functions+=( vcs_info )
setopt prompt_subst
# %b: branch
# %u: unstaged changes
zstyle ':vcs_info:git:*' formats '%F{5}(%b%u)%f '
# this makes %u work, but also the prompt is clearly slower in git dirs when this is on
zstyle ':vcs_info:*' check-for-changes true
# what string to use for %u when there are unstaged changes
zstyle ':vcs_info:*' unstagedstr '*'
# vcs_info supports multiple version control systems, but I need just git
zstyle ':vcs_info:*' enable git

# Explaining prompt:
#
# %B /  %F{n}: begin bold / color
# %b / %f: end bold / color
# %n~: display n latest directories of current directory
# %#: display a '%' (or '#' when root)
# %(..): conditional expression (see docs)
# %?: exit code of last process
# %n@%m: user@host
PROMPT='%B${vcs_info_msg_0_}%F{12}%2~%f %# %b'
# rprompt is located on the right side of the terminal
RPROMPT='%(?..%F{red}(%?%) %f)%n@%m'

HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
bindkey -v

# The following lines were added by compinstall
zstyle :compinstall filename '$HOME/.zshrc'

autoload -Uz compinit
compinit

# User configuration

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
    export EDITOR='nvim'
else
    export EDITOR='nvim'
fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Path to Shell Scripts
export PATH=$PATH:/home/pkyle/.scripts
export TERM=kitty

#############################
#          ALIASES          #
#############################
# Git Tracked Dotfiles Directory
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

# Bitwarden
alias bu='export BW_SESSION="$(bw unlock --raw)" && echo -e "\033[32mYour vault is now unlocked!"'

# Battery
if [[ -f "/sys/class/power_supply/BAT1/capacity" ]]; then
        alias battery="echo Battery: $(cat /sys/class/power_supply/BAT0/capacity)% $(cat /sys/class/power_supply/BAT1/capacity)%"
elif [[ -f "/sys/class/power_supply/BAT0/capacity" ]]; then
        alias battery="echo Battery: $(cat /sys/class/power_supply/BAT0/capacity)%"
fi

# Shell Scripts
alias fm='fm.sh' 

# Utilities
alias ls='ls --color=auto --group-directories-first'
alias la='ls -a'
alias rm='rm -i'
alias grep='grep --color=auto'
alias ssh='kitty +kitten ssh'
alias wttr='curl wttr.in/Rowville\?format=3'
alias webserv='python3 -m http.server'
alias src='omz reload'
alias clock="tty-clock -c"
