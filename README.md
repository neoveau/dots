# Neoveau Dotfiles
Dotfiles for BSPWM and Sway on Arch Linux

![NCMPCPP and Neofetch](/dots1.png)
![Neovim](/dots2.png)

## Dependencies

```bash
git zsh xorg xwallpaper xorg-xinit bspwm sxhkd rofi dunst
```

## Cloning this repository
https://www.atlassian.com/git/tutorials/dotfiles

```bash
curl -Lks http://gitlab.com/neoveau/dots/install-config | /bin/bash
```

```bash
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
```
